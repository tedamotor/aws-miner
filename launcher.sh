#!/bin/bash

#chmod all the scripts
chmod +x updateUserDataWithMinerConfig.sh
chmod +x updateSpotFleetConfig.sh
chmod +x minerHashChecker.sh
chmod +x fleetManager.sh
chmod +x installPrereqs.sh
chmod +x configureAWS.sh

#Install prerequisites and libraries for running
./installPrereqs.sh

#Configure the correct AWS account
./configureAWS.sh

while true; do
  #Pull in each of the regions that we will be running on
  ARR=$(curl "https://api.airtable.com/v0/appRF35xHzyRZR7Bl/tbltP8P0iS10ovKMu?filterByFormula=NOT(%7BRun%7D+%3D+'')" -H "Authorization: Bearer keygD9Z2v7ErMlYjA" | jq '.records[]' | jq -r '.fields.zone')

  #loop through the regions and launch spot fleet
  for X in ${ARR[@]}
  do
    export X
    #pull coin name for region from airtable and update the user data with correct miner config
    ./updateUserDataWithMinerConfig.sh >> updateMinerConfig.txt

    #pull in spot fleet info for region from airtable and update fleet-config
    ./updateSpotFleetConfig.sh >> updateSpotFleetConfig.txt

    # run spot fleet with AWS CLI
    ACTIVE_FLEETS=$(aws ec2 describe-spot-fleet-requests --query "SpotFleetRequestConfigs[?SpotFleetRequestState == 'active' ]" --region ${X} | jq -r '.[]' | jq -r '.SpotFleetRequestId')
    if [ -z "$ACTIVE_FLEETS" ]; then
      aws ec2 request-spot-fleet --spot-fleet-request-config file:///root/${X}-config.json --region ${X} >> /root/log.txt
    fi
  done

  ./fleetManager.sh
done

# kill droplet
#wget https://github.com/digitalocean/doctl/releases/download/v1.16.0/doctl-1.16.0-linux-amd64.tar.gz -O /root/doctl-1.16.0-linux-amd64.tar.gz
#tar xf /root/doctl-1.16.0-linux-amd64.tar.gz -C /usr/bin
#rm /root/doctl-1.16.0-linux-amd64.tar.gz
#doctl auth init -t 73a4acaea793498b9236fc9c812a4fd7df3f44c886d05d84588ceecc0c14f39a
#ID=$(hostname)
#doctl auth init -t 73a4acaea793498b9236fc9c812a4fd7df3f44c886d05d84588ceecc0c14f39a
#doctl compute droplet delete $ID -f
