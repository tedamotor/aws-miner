#!/bin/bash

#prereq
sudo apt update -yq
sudo apt-get update -yq
sudo apt-get upgrade -yq
sudo apt-get install git build-essential cmake libuv1-dev libmicrohttpd-dev libssl-dev wget software-properties-common htop jq athena-jot bc dos2unix cpulimit curl -y
snap install jq
sudo apt-get install -y python2.7 -yq
sudo apt-get install -y python3 -yq
sudo apt-get install -y python3-pip
sudo apt-get install -y python-pip
sudo apt-get install build-essential libssl-dev libffi-dev python-dev -yq
sudo apt install awscli
snap install aws-cli --classic
