#!/bin/bash
#Get all the regions that are running
while true; do
  sleep 50
  REGIONS=$(curl "https://api.airtable.com/v0/appRF35xHzyRZR7Bl/tbltP8P0iS10ovKMu?filterByFormula=NOT(%7BRun%7D+%3D+'')" -H "Authorization: Bearer keygD9Z2v7ErMlYjA" | jq '.records[]' | jq -r '.fields.zone')
  for REGION in ${REGIONS[@]}
  do
    INSTANCES_CANCEL=""
    ACTIVE_INSTANCES=$(aws ec2 describe-instances --region ${REGION} --filters "Name=instance-state-code,Values=16" --query 'Reservations[*].Instances[*].[InstanceId]')
    ACTIVE_INSTANCES="${ACTIVE_INSTANCES//[}"
    ACTIVE_INSTANCES="${ACTIVE_INSTANCES//]}"
    ACTIVE_INSTANCES=${ACTIVE_INSTANCES//\"/}
    ACTIVE_INSTANCES=${ACTIVE_INSTANCES//,}
    counter=0
    for INSTANCE in ${ACTIVE_INSTANCES[@]}
    do
      MINER_LATEST_INFO=$(curl "https://uplexa.poolbux.com/api/miner/UPi1S1uqRRNSgC26PjasZP8FwTBRwnAEmBnx5mAYsbGqRvsU46aficYEA3FAT621EuPeChyKQumS7j6jpF74zW9tLJMvYSzTnWF5Uz9bZP9fX/chart/hashrate/${INSTANCE}"  | jq -r '.[0]')
      if [ -z "$MINER_LATEST_INFO" ]; then
        MINER_LATEST_HS=0
      else
        MINER_LATEST_TS=$(jq -n "$MINER_LATEST_INFO" | jq -r .ts)
        MINER_LATEST_HS=$(jq -n "$MINER_LATEST_INFO" | jq -r .hs)
      fi

      if [ -z "$MINER_LATEST_INFO" ]; then
        AWS_AGE=$(aws ec2 describe-instances --region ${REGION} --instance-ids ${INSTANCE} | jq -r .Reservations | jq -r .[] | jq -r .Instances | jq -r .[] | jq -r .LaunchTime)
        START_TIME=$(date -d $AWS_AGE +"%s")
        MINER_AGE=$(echo "$(date +"%s") - ($START_TIME)" | bc -l | awk '{printf "%.0f", $0}' )
      else
        MINER_AGE=$(echo "$(date +"%s") - ($START_TIME)" | bc -l | awk '{printf "%.0f", $0}' )
      fi

      HS_THRESHOLD=3750
      AGE_THRESHOLD=400 # seconds (5 minutes)

      MINER_CONNECTION_STATUS=$(if [ -z "$MINER_LATEST_INFO" ]; then echo "OFF"; else echo "ON"; fi)
      MINER_COMPARE_AGE=$(if [ -z "$MINER_AGE" ]; then echo 1000000; else echo $MINER_AGE; fi)

      if [ $MINER_COMPARE_AGE -gt $AGE_THRESHOLD ]; then
        if [ "$MINER_CONNECTION_STATUS" = "OFF" ]; then
          INSTANCES_CANCEL+=" ${INSTANCE}"
          counter=$((counter+1))
        else
          if [ $MINER_LATEST_HS -lt $HS_THRESHOLD ]; then
            INSTANCES_CANCEL+=" ${INSTANCE}"
            counter=$((counter+1))
          fi
        fi
      fi
      if [ $counter -gt 500 ]; then
        aws ec2 terminate-instances --region ${REGION} --instance-ids ${INSTANCES_CANCEL}
        counter=0
        INSTANCES_CANCEL=""
      fi
    done
    aws ec2 terminate-instances --region ${REGION} --instance-ids ${INSTANCES_CANCEL}
  done

done
